package com.example.contactphone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Button btnSub, btnList;

    private static int compteur = 0;
    public ArrayList<String> listContact = new ArrayList<String>();
    private final static String CLE_INCREMENT = "save";
    private final static String CLE_LIST = "list";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("myApp", "onCreate");
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            Log.d("myApp","inCondition");

            int val = savedInstanceState.getInt(CLE_INCREMENT);
            Toast.makeText(this,"valeur return : "+val, Toast.LENGTH_LONG).show();
        }
        setContentView(R.layout.activity_main);

        btnSub = findViewById(R.id.btnSubmit);
        btnList = findViewById(R.id.btnLis);
        btnSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText inputnom = findViewById(R.id.editNom);
                String nom = inputnom.getText().toString();
                EditText inputPrenom = findViewById(R.id.editprenom);
                String prenom = inputPrenom.getText().toString();
                EditText inputPhone = findViewById(R.id.editPhone);
                String phone = inputPhone.getText().toString();
                listContact.add(nom+"_"+prenom+"_"+phone);
                Toast.makeText(MainActivity.this,"Contact ajouté", Toast.LENGTH_LONG).show();
            }
        });
        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                intent.putStringArrayListExtra("list", listContact);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onSaveInstanceState(Bundle saveInstanceState) {
        super.onSaveInstanceState(saveInstanceState);
        compteur += 1;
        saveInstanceState.putInt(CLE_INCREMENT,compteur);
        saveInstanceState.putStringArrayList(CLE_LIST,listContact);
        Toast.makeText(MainActivity.this,"valeur save : "+saveInstanceState.getInt(CLE_INCREMENT), Toast.LENGTH_LONG).show();


    }

    @Override
    protected void onRestoreInstanceState(Bundle saveInstanceState) {
        super.onRestoreInstanceState(saveInstanceState);
        compteur = saveInstanceState.getInt(CLE_INCREMENT);
        listContact = saveInstanceState.getStringArrayList(CLE_LIST);
    }
}
