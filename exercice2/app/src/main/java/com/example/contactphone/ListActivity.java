package com.example.contactphone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    public ArrayList<String> listContact = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle listSavedInstanceState) {
        Button btnBack;

        super.onCreate(listSavedInstanceState);
        setContentView(R.layout.activity_list);

        listContact = getIntent().getStringArrayListExtra("list");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                listContact
        );
        ListView listView = findViewById(R.id.listContact);
        listView.setAdapter(adapter);

        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener()
         {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListActivity.this, MainActivity.class);

                startActivity(intent);
            }
        });
    }
    @Override
    protected void onSaveInstanceState(Bundle saveInstanceState) {
        super.onSaveInstanceState(saveInstanceState);
        saveInstanceState.putStringArrayList("list",listContact);
    }


}
