package com.example.contactphone;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Button btnBack;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        ArrayList<String> listContact = getIntent().getStringArrayListExtra("listContact");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                listContact
        );
        ListView listView = findViewById(R.id.listContact);
        listView.setAdapter(adapter);

        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener()
         {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
