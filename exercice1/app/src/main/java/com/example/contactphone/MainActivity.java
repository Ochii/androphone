package com.example.contactphone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Button btnSub, btnList;
    public ArrayList<String> listContact = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSub = findViewById(R.id.btnSubmit);
        btnList = findViewById(R.id.btnLis);
        btnSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText inputnom = findViewById(R.id.editNom);
                String nom = inputnom.getText().toString();
                EditText inputPrenom = findViewById(R.id.editprenom);
                String prenom = inputPrenom.getText().toString();
                EditText inputPhone = findViewById(R.id.editPhone);
                String phone = inputPhone.getText().toString();
                listContact.add(nom+"_"+prenom+"_"+phone);
                Toast.makeText(MainActivity.this,"Contact ajouté", Toast.LENGTH_LONG).show();
            }
        });
        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                intent.putStringArrayListExtra("listContact", listContact);
                startActivity(intent);
            }
        });
    }
}
