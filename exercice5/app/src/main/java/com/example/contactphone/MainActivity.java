package com.example.contactphone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Button btnSub, btnList, btnFile, btnEraseFile, btnEraseBdd, btnSync;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSub = findViewById(R.id.btnSubmit);
        btnList = findViewById(R.id.btnLis);
        btnFile = findViewById(R.id.btnFile);
        btnEraseFile = findViewById(R.id.btnEraseFile);
        btnEraseBdd = findViewById(R.id.btnEraseBdd);
        btnSync = findViewById(R.id.btnSync);

        btnSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactBdd contactBdd = new ContactBdd(MainActivity.this);
                contactBdd.openBdd();
                EditText inputnom = findViewById(R.id.editNom);
                String nom = inputnom.getText().toString();
                EditText inputPrenom = findViewById(R.id.editprenom);
                String prenom = inputPrenom.getText().toString();
                EditText inputPhone = findViewById(R.id.editPhone);
                String phone = inputPhone.getText().toString();
                String ajout = nom + "_" + prenom + "_" + phone + "\n";
                Toast.makeText(MainActivity.this, "Contact ajouté", Toast.LENGTH_LONG).show();
                FileOutputStream fos = null;
                try {
                    Log.d("myApp", "try ...");
                    fos = openFileOutput("contacts.txt", Context.MODE_APPEND);
                    fos.write(ajout.getBytes());
                    fos.close();
                } catch (FileNotFoundException e) {
                    Log.d("myApp", "ba non ...");
                    Toast.makeText(MainActivity.this, "Settings not saved", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    Log.d("myApp", "ba non ...");
                    Toast.makeText(MainActivity.this, "Settings not saved", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> contacts = getBdd();
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                intent.putStringArrayListExtra("listContact", contacts);
                startActivity(intent);
            }
        });
        btnFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> data = getFileContent();
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                intent.putStringArrayListExtra("listContact", data);
                startActivity(intent);
            }
        });
        btnEraseFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeToFile("", MainActivity.this);
            }
        });
        btnEraseBdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactBdd contactBdd = new ContactBdd(MainActivity.this);
                contactBdd.openBdd();
                contactBdd.removeAll();
            }
        });
        btnSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactBdd contactBdd = new ContactBdd(MainActivity.this);
                ArrayList<String> data = getFileContent();
                // Envoie de la liste des contacts récupérés dans le fichier txt
                contactBdd.syncFileAndDb(data);
            }
        });
    }

    private String readFromFile(Context context) {
        String ret = "";
        try {
            InputStream inputStream = context.openFileInput("contacts.txt");
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append("\n").append(receiveString);
                }
                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }
        return ret;
    }

    private void writeToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("contacts.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private ArrayList<String> getBdd() {
        ContactBdd contactBdd = new ContactBdd(MainActivity.this);
        contactBdd.openBdd();
        ArrayList<String> contacts = contactBdd.getContacts();

        return contacts;
    }

    private ArrayList<String> getFileContent() {
        String str = readFromFile(MainActivity.this);
        String[] listContact = str.split("\n", 0);
        ArrayList<String> data = new ArrayList<>();
        for (String element : listContact) {
            data.add(element);
        }

        return data;
    }
}
