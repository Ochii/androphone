package com.example.contactphone;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class ContactBdd {

    private static final int BASE_VERSION = 1;
    private static final String BASE_NOM = "contact.db";

    private static final String TABLE_CONTACTS = "table_contacts";
    private static final String COLONNE_ID = "id";
    private static final int NUM_COLONNE_ID = 0;
    private static final String COLONNE_NOM="nom";
    private static final int NUM_COLONNE_NOM = 1;
    private static final String COLONNE_PRENOM="prenom";
    private static final int NUM_COLONNE_PRENOM = 2;
    private static final String COLONNE_PHONE="phone";
    private static final int NUM_COLONNE_PHONE = 3;
    public static final String REQUETE_CREATION_BD="create table "
            + TABLE_CONTACTS + " ("
            + COLONNE_ID + "integer primary key autoincrement, "
            + COLONNE_NOM + " text not null, "
            + COLONNE_PRENOM + " text not null, "
            + COLONNE_PHONE + " text not null );";

    private MaBaseDeDonnee maBaseDeDonnee;
    private SQLiteDatabase bdd;

    public ContactBdd(Context context) {
        maBaseDeDonnee = new MaBaseDeDonnee(context,BASE_NOM, null, BASE_VERSION);

    }
    public void openBdd() {
        bdd = maBaseDeDonnee.getWritableDatabase();
    }

    public void closeBdd() {
        bdd.close();
    }

    public long insertContact(String nom, String prenom, String phone) {
        ContentValues values = new ContentValues();

        values.put(COLONNE_NOM, nom);
        values.put(COLONNE_PRENOM, prenom);
        values.put(COLONNE_PHONE, phone);

        return bdd.insert(TABLE_CONTACTS, null, values);
    }

    public ArrayList<String> getContacts() {
        ArrayList<String> contacts = new ArrayList<String>();
        Cursor c = bdd.rawQuery("SELECT * FROM "+TABLE_CONTACTS,null);
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                String contactNom = c.getString(1);
                String contactPrenom = c.getString(2);
                String contactPhone = c.getString(3);
                String concatInformations = contactNom+"_"+contactPrenom+"_"+contactPhone+"\n";
                contacts.add(concatInformations);
                c.moveToNext();
            }
        }
        return contacts;
    }

    public void removeAll() {
        bdd.delete(TABLE_CONTACTS, null,null);
    }

    public void syncFileAndDb(ArrayList<String> fileContent) {
        Log.d("myApp", "dans le sync");
        for (String elements: fileContent) {
            if (!elements.isEmpty()) {
                Log.d("myApp", "dans la boucle");

                String[] element = elements.split("_",0);
                Log.d("myApp", "apres le split");
                Log.d("myApp", element[0]);
                Log.d("myApp", element[1]);
                Log.d("myApp", element[2]);

                searchContact(element[0],element[1],element[2]);
            }

        }
    }

    public void searchContact(String nom, String prenom, String phone) {
        Log.d("myApp", "dans le search : debut");
        Log.d("myApp", "nom :"+nom);
        Log.d("myApp", "prenom : "+prenom);
        Log.d("myApp", "phone : "+phone);
        openBdd();
        String[] columns = new String[] { COLONNE_NOM, COLONNE_PRENOM, COLONNE_PHONE };
        String whereClause = COLONNE_NOM + " = ?  AND " + COLONNE_PRENOM + " = ?  AND " + COLONNE_PHONE + " = ? ";
        String[] params = new String[] { nom, prenom, phone };
        Cursor c = bdd.query(TABLE_CONTACTS, columns, whereClause, params, null, null, null);
        Log.d("myApp", "dans le search : après la query : "+c.getCount());

        if (c.getCount() == 0) {
            Log.d("myApp", "dans le search : condition if");

            insertContact(nom,prenom,phone);
            c.close();
        }
        c.close();
    }
}
