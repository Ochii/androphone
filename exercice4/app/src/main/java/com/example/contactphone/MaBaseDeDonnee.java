package com.example.contactphone;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class MaBaseDeDonnee extends SQLiteOpenHelper {

    private static final String TABLE_CONTACTS = "table_contacts";
    private static final String COLONNE_ID = "id";
    private static final String COLONNE_NOM="nom";
    private static final String COLONNE_PRENOM="prenom";
    private static final String COLONNE_PHONE="phone";
    public static final String REQUETE_CREATION_BD="create table "
            + TABLE_CONTACTS + " ("
            + COLONNE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLONNE_NOM + " text not null, "
            + COLONNE_PRENOM + " text not null, "
            + COLONNE_PHONE + " text not null );";


    public MaBaseDeDonnee(Context context, String nom,
                     SQLiteDatabase.CursorFactory cursorFactory , int version) {
        super(context, nom, cursorFactory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(REQUETE_CREATION_BD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table " + TABLE_CONTACTS + ";");
        onCreate(db);
    }


}
