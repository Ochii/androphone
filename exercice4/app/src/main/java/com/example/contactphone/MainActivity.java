package com.example.contactphone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Button btnSub, btnList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        ContactBdd contactBdd = new ContactBdd(this);
//        contactBdd.openBdd();
        btnSub = findViewById(R.id.btnSubmit);
        btnList = findViewById(R.id.btnLis);
        btnSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactBdd contactBdd = new ContactBdd(MainActivity.this);
                contactBdd.openBdd();
                EditText inputnom = findViewById(R.id.editNom);
                String nom = inputnom.getText().toString();
                EditText inputPrenom = findViewById(R.id.editprenom);
                String prenom = inputPrenom.getText().toString();
                EditText inputPhone = findViewById(R.id.editPhone);
                String phone = inputPhone.getText().toString();
                contactBdd.insertContact(nom,prenom,phone);
                Toast.makeText(MainActivity.this,"Contact ajouté", Toast.LENGTH_LONG).show();
            }
        });
        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactBdd contactBdd = new ContactBdd(MainActivity.this);
                contactBdd.openBdd();
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                ArrayList<String> contacts = contactBdd.getContacts();
                intent.putStringArrayListExtra("listContact",contacts);
                startActivity(intent);
            }
        });
    }
}
